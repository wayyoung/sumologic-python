# -*- coding: utf-8 -*-
"""
Created on Tue Sep 18 21:35:32 2018
@author: kung_c
=============================== EDIT HISTORY ==================================
DATE        INITIAL     CONTENTS
===============================================================================
20180918    ck          initial version

To use this script, please
1. install sumologic by the setup.py at the top of this project
2. install boto3 for AWS s3 
3. install ConfigParser
4. create the correct ~/.aws/crendentials
5. create the correct ~/.sumo/config 
"""

from sumologic import sumologic 
import time
from datetime import datetime
from datetime import timedelta
import json
import boto3
import ConfigParser 
import os, sys, errno, subprocess

s3_crash_bucket="wd-portal-prod-fwcrashdump"

def mkdir_p(path):
    try:
        os.makedirs(path,0755)
    except OSError as exc:
        if exc.errno==errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise
        

def download_s3_file(bucket,s3path,full_path_to_save):
    #ret=subprocess.check_output(["aws","s3","cp","s3://"+bucket+"/"+s3path,full_path_to_save])
    #print(ret)
	s3.Bucket(bucket).download_file(s3path,full_path_to_save)

s3 = boto3.resource('s3')


conf = ConfigParser.SafeConfigParser()  
conf.read(os.path.expanduser('~')+'/.sumo/config')  
sumo_id  = conf.get('default','sumo_access_id') 
sumo_key = conf.get('default','sumo_access_key')
sumo_ep  = conf.get('default','sumo_endpoint') 

# Conditions to query 
#sourcename='e7e6f19ce2ee15b22f4cb3211a22e7a9'
crash_type='tombstones'
download_dir='download/'+crash_type
LIMIT = 20000
from_time = '2019-05-17T00:00:00'
to_time   = '2019-05-29T00:00:00'
time_zone = 'UTC'
by_receipt_time = False
delay = 5
#q = '_sourcename="'+sourcename+'" AND crashreport'
#q = '(_sourcename="bffe87df01fc70c9bfc4fa753b71e1fd" or _sourcename="9f4af50a4911948c35fc45029bbc4517") And crashreport'
#q = 'crashreport AND fw_version=5.7.* AND '+crash_type
q='"crashreport" AND "'+crash_type+'" NOT "ffmpeg" AND _sourcecategory=*yoda*| where fw_version >= "5.7.0"'

mkdir_p(download_dir)

sumo = sumologic.SumoLogic(sumo_id, sumo_key, sumo_ep)
sj = sumo.search_job(q, from_time, to_time, time_zone, by_receipt_time)

status = sumo.search_job_status(sj)
while status['state'] != 'DONE GATHERING RESULTS':
    print('.')
    if status['state'] == 'CANCELLED':
        break
    time.sleep(delay)
    status = sumo.search_job_status(sj)

print(status['state'])

if status['state'] == 'DONE GATHERING RESULTS':
    count = status['messageCount']
    limit = count if count < LIMIT and count != 0 else LIMIT # compensate bad limit check
    r = sumo.search_job_messages(sj, limit=limit)
    print(count)
    count = count if count < LIMIT and count != 0 else LIMIT # compensate bad limit check
    download_count=0
    for msg in r["messages"]:
        dumpfile=json.loads(msg['map']['_raw'])['dumpfile']
        try:
            s3file=dumpfile[dumpfile.index("data/wd"):] 
            local_file=s3file[(s3file.rindex("/")+1):]
            tokens=local_file.split('_')

            ym=time.strftime("%Y%m",time.gmtime(int(tokens[0])))
            sn=msg['map']['_sourcename'].replace(':','-')
            dest_dir=(download_dir+'/'+sn+'/'+ym) 
            mkdir_p(dest_dir) 
        except Exception:
            try:
                # this is the case that the dest_dir may not be able to get created, download the file into download_dir directly
                download_s3_file(s3_crash_bucket,s3file,download_dir+'/'+local_file)

                download_count+=1
                print(str(download_count)+'/'+str(count)+':'+s3file) 
            except Exception:
                print("ERROR!! dumpfile:"+dumpfile)
        else:
            try:
                download_s3_file(s3_crash_bucket,s3file,dest_dir+'/'+local_file)
                download_count+=1
                print(str(download_count)+'/'+str(count)+':'+s3file) 
            except Exception as ex:
                print "ERROR_1!! dumpfile:"+dumpfile


        


