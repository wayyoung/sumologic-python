import os, re


FILTER_reboot='reboot: [DEBUG] kernel_restart'

def process_file(file,collector):
    try:
        fin=open(file,'rt')
        txt=fin.read()
        fin.close()
        if txt.find(FILTER_reboot) < 0:
        #m=re.search(FILTER_reboot,txt)
        #if m:
            if collector :
            	collector.write('>>>> '+file)
            	collector.write(txt)
            	collector.write('<<<<'+'\n\n')
            	
    except Exception,err:
        print("Error while process_file: "+file)
        print(err)    



def process_directory(directory,collector):
    for root,dirs,files in os.walk(directory):
        for f in files:
            process_file(os.path.join(root,f),collector)
        for d in dirs:
            process_directory(os.path.join(root,d),collector)    


with open('check.txt','wt') as fw:
	process_directory('download', fw)